import { createStore } from 'redux'

const startState = {

  notes:[ {
    id: 1,
    title: 'First',
    text: 'First-one'
  },
  {
  id: 2,
    title: 'Second',
      text: 'Second-one'
},
{
  id: 3,
    title: 'Third',
      text: 'Third-one'
}
  ]
}


function reducer(state = startState, action) {
  const { type, payload } = action
  switch (type) {


    default: return state
  }
}

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
export default store