import React from 'react';
import { NavLink } from 'react-router-dom'
import './style.scss'


export const Header = props => {
  return(
<header>
  <nav>
    <NavLink to='/' exact >Home</NavLink>
    <NavLink to='/about' >About page</NavLink>
    <NavLink to='/note' >Note page</NavLink>

  </nav>
</header>

  )
}