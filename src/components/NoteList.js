import React from 'react'
import {SingleNote} from './SingleNote'

export const NoteList = ({notes}) => {
  const list = notes.map((elem, index)=>{
    return (<SingleNote  key={elem.id} title={elem.title} text={elem.text} id={index}/>)
  })
  return (
    <div>{list}</div>
  )
}