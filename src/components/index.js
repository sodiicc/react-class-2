export {HomePage} from './HomePage'
export {About} from './About'
export {Header} from '../commons/Header'
export {Notes} from './Notes'