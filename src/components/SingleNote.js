import React from 'react'
import { Link } from 'react-router-dom'


function validId(id, title){
  let path = `/note/${id}`
  if(+id==0 || id){
    return (<Link to={path}>{title}</Link>)
  }
  return (title)
}

export const SingleNote = ({title, text, id}) => {
    
  return (
      <div style={{
        padding: '15px',
        border: '1px solid blue' ,
        backgroundColor: '#ccc'
      }}>
        <h2>{validId(id, title)}</h2>

        <h3>{text}</h3>
      </div>
  )
}