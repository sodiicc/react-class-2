import React from 'react';
import {connect} from 'react-redux'
import {SingleNote} from './SingleNote'
import {Route} from 'react-router-dom'
import {NoteList} from './NoteList'

function mapStateToProps(store){
  return {...store}
}

export const Notes = connect(mapStateToProps)((props) => {
  return (
  <div>
    <Route path="/note" exact render={()=>{
      return <NoteList notes={props.notes} />
    }} />
    <Route path='/note/:index' render={()=>{
      let i = props.match.params.index
      return <SingleNote
      title={props.notes[i].title} text={props.notes[i].text} />
    }} />    
  </div>
  )
})