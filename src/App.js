import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { HomePage, About, Header, Notes } from './components'
import store from './redux'
import { Provider } from 'react-redux'
import { SingleNote } from './components/SingleNote';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Header />
            {/* <Notes /> */}
          <Switch>
            <Route path="/" exact component={HomePage} />
            <Route path="/about" component={About} />
            <Route path="/note" exact component={Notes} />
            <Route path="/note/:index" exact component={Notes} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
